﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;

namespace OnlineShopAutomationTest
{
    [TestFixture]
    public class TestAddToCart
    {
        IWebDriver _driver;
        private string _productName;
        private By _searchField;
        private By _searchButton;
        private By _productDetailLink;
        private By _closeModalButton;
        private By _cartProductLink;
        private By _openCartButton;

        [OneTimeSetUp]
        public void Initialize()
        {
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArgument("--start-maximized");

            _driver = new ChromeDriver(chromeOptions);
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            _driver.Navigate().GoToUrl("http://automationpractice.com/index.php");

            _productName = "Faded Short Sleeve T-shirts";
            _searchField = By.Name("search_query");
            _searchButton = By.Name("submit_search");
            _productDetailLink = By.XPath("//div[@class='product-image-container']/a[@title='" + _productName + "']");
            _closeModalButton = By.ClassName("cross");
            _cartProductLink = By.LinkText(_productName);
            _openCartButton = By.XPath("//a[@title='View my shopping cart']");

        }

        [Test]
        public void GIVEN_ProductNameToSearch_WHEN_AdddingToCartWithinTheProductDetails_THEN_AddToCart()
        {
            var addButton = By.Name("Submit");

            _driver.FindElement(_searchField).SendKeys(_productName);
            _driver.FindElement(_searchButton).Click();
            _driver.FindElement(_productDetailLink).Click();
            _driver.FindElement(addButton).Click();
            _driver.FindElement(_closeModalButton).Click();
            _driver.FindElement(_openCartButton).Click();
            var isProductAdded = _driver.FindElement(_cartProductLink).Displayed;

            if (!isProductAdded)
            {
                throw new Exception("Product not added to cart");
            }
        }

        [Test]
        public void GIVEN_ProductNameToSearch_WHEN_AdddingToCartFromSearchResult_THEN_AddToCart()
        {
            var addButton = By.XPath("//a[@title='Add to cart']");
            var actions = new Actions(_driver);


            _driver.FindElement(_searchField).SendKeys(_productName);
            _driver.FindElement(_searchButton).Click();
            actions.MoveToElement(_driver.FindElement(_productDetailLink)).Build().Perform();
            _driver.FindElement(addButton).Click();
            _driver.FindElement(_closeModalButton).Click();
            _driver.FindElement(_openCartButton).Click();
            var isProductAdded = _driver.FindElement(_cartProductLink).Displayed;

            if (!isProductAdded)
            {
                throw new Exception("Product not added to cart");
            }
        }


        [TearDown]
        public void Dispose()
        {
            _driver.Dispose();
        }

    }
}
